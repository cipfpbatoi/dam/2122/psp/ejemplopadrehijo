import java.util.Locale;
import java.util.Scanner;

public class Hijo {

    public static void main(String args[])
    {
        System.out.println("Parámetros pasados al hijo: ");

        for (String arg : args){
            System.out.print(arg + " ");
        }

        System.out.println();

        Scanner sc = new Scanner(System.in);
        String linea;

        do {

            linea = sc.nextLine();

            try {
                double numero = Double.parseDouble(linea);
                System.out.println("El número " + numero +
                        " al cuadrado es: " + numero*numero);
            } catch (NumberFormatException e){
                System.err.println("Prueba otra vez");
            }

        }while (!linea.toLowerCase().equals("stop"));
    }

}
