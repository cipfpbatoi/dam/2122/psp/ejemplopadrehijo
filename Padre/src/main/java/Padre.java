import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Padre {

    public static void main (String args[]) throws IOException, InterruptedException {
        System.out.println("Parámetros pasados al padre: ");
        Arrays.stream(args).sequential().forEach( s -> System.out.print(s + " "));
        System.out.println("");

        List<String> parametros = new ArrayList<>();

        parametros.add("java");
        parametros.add("-jar");
        parametros.add("Hijo.jar");

        for (String arg : args ){
            parametros.add(arg);
        }

        ProcessBuilder pb = new ProcessBuilder(parametros);
        pb.inheritIO();

        Process hijo = pb.start();
        hijo.waitFor();

    }

}
